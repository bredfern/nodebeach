title: Nodebeach Apps

date: 2015-05-31 03:10:51
-------------------------

##Pyscho Flute

Based on the csound android library this open source app uses a physical model of a flute passed through a low pass filter to make something that sounds a lot like guitar feedback.

<a href="https://play.google.com/store/apps/details?id=com.magickhack.psychoflute&hl=en">Get it now</a> for free from Google Play.

##Moon Synth

Not a DX7 but its based on the same idea - fm synthesis. But in this case its pushed past the normal parameters to create a strange spacy, randomized sound effect.

<a href="https://play.google.com/store/apps/details?id=la.noise.moonsynth&hl=en">Download it now</a> for free from Google Play.

While both of these are native java I'm moving into working with web audio for my upcoming app LoveDAW which will run on chromsos android 5+ and ios 7+.
