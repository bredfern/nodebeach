title: Meteor Versus Rails
date: 2015-05-30 18:05:02
tags:
---
The reason I posit that Meteor is the new rail is just this, while in the days of server rendered pages Rails was the king of agility Meteor should be ready to take away its crown.

The problem for Rails is that you wind up using it as an api layer, need a seperate scripting layer, have to integrate with coffeescript, which can be time consuming and even akward with angularjs. Then you are likely having to use angularjs for industry reasons.

You could also use backbone or ember or react, but the bottom line is 
that you have one set of code for your client and one set for the server.

If you are our publishing out to mobile as a hybrid app you're going to hope to the heavens than some junior level newbie didn't lock the application into server side views or you have some major refactoring to do.

All that being said if you are integrating with relational data stores this may be one of only a few options. You could use loopback to generate a pure server side api and then use one of the client side MVC frameworks. Just migrating to Mongo/Meteor is not a one size fits all solution.

If you need to integrate with relational data stores then Rails can be a good choice if you already have Ruby developers or plan to hire Ruby developers.

You may find that loopback is a better choice if you are using or planning to hire Node rather than Rails developers but your application requires 

Where Meteor shines is in the startup or new application scenario where the choice of data storage has not yet been made, you are starting with a basic idea and need to get to a working MVP as fast as you possibly can.

What makes Meteor so special? It combines the kind of rails generator approach with isomorphic development, so your same routers and models and views are used on both or either of the server or the client.

That's huge. I'll get more into detail in future posts, but the amount of pain this saves is incalculabe compared to the complexity of multiple layers. While this seems like it would lead to a monolithic approach I'll break down how to use Meteor with microservices in future post.

This doesn't have to be an all or nothing choice, its actually not hard to use Metoer along with rails based microservices.

But for startups, Meteor is a tool you should look into, to say the least!

