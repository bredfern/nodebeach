title: Using Hexo

date: 2015-06-01 00:00:33

tags:
-----

For those who like to do everything with Javascript, the <a href="https://hexo.io/">Hexo</a> static generator is pretty sweet.

Being relatively new it does have its quirks, you will need to manually add your CNAME file to you public directory for example if you want to use a custom domain name on github pages. Hint, you want to stick your CNAME file into the source directory.

Not that I have anything against Ruby its a fine language, but only having so many hours in the day I chose to master Javascript even if the industry is currently going ga ga for Ruby.

I plan on writing some extensions, for being able to embed cool things in the page such as web audio poylmer widgets would be cool.

But for what it is its great. I like being able to only look at Javascript code and not get caught up with learning other stuff. Just getting my feet wet with Es6 is enough.

However es6 is close enough to Rust to have gotten up my interest in that. When I make something interesting with Rust I can blog about that otherwise I'm talking about Hexo today lol.

It doesn't use less or sass, out of the box we use stylus so I may find myself changing that at some point but for now stylus is working great and it took me all of 5 minutes to create my own custom theme using stylus.
