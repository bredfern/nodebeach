title: Es6 and Rust

date: 2015-04-30 00:48:38

tags:
-----

The most excting part about Es6 and Rust are the pieces that are in common. Both of them share the fundamental concept of "let". Of course Rust gets into a lot of stuff that you won't touch in Javascript, such as memory management, etc... But the sharing of syntax makes it easy to start with Javascript and then move into system level development with Rust.

This <a href="https://www.codementor.io/rust/tutorial/steve-klabnik-rust-vs-c-go-ocaml-erlang">article</a> does a better job of breaking down the similarities and differences between Rust and Javascript. But as a seasoned Javascript developer getting his feet wet with Rust I do find it exciting.
